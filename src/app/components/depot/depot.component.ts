import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-depot',
  templateUrl: './depot.component.html',
  styleUrls: ['./depot.component.scss']
})
export class DepotComponent implements OnInit {
  lat: number = 48.8421954;
  lng: number = 2.3475915;
  constructor() { }

  ngOnInit(): void {
  }

}
