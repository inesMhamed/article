import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { Tarifications } from 'src/app/models/tarification.model';
import { TarificationsService } from 'src/app/shared/tarifications.service';
import { first } from "rxjs/operators";

@Component({
  selector: 'app-tarification',
  templateUrl: './tarification.component.html',
  styleUrls: ['./tarification.component.scss'],
  providers: [TarificationsService]
})
export class TarificationComponent implements OnInit {

  newTarifications!:Tarifications
  tarificationRamassage: Tarifications[]=[]
  type: number
  page: number
  limit: any
  pageSize: number;
  tarificationForm!: FormGroup;
  submitted = false;

  listTarifications: any[] = [];
  constructor(private tarificationService: TarificationsService, private formBuilder: FormBuilder) {
    this.pageSize = 10;
    this.page = 1;
    this.type = 1
  }
  get f() {
    return this.tarificationForm.controls;
  }

  ngOnInit(): void {
    this.getlistTarifications();
    this.getTarificationBytype(this.type)

    this.tarificationForm = this.formBuilder.group({
      intitule: [''],
      type: [''],
      tarificationliaison: [''],
    });
    
    
  }
  onSubmit():void {

    this.submitted = true;
    if (this.tarificationForm.invalid) {
      return;
  }
    this.tarificationService.addTarification(this.newTarifications).pipe(first()).subscribe((res) => {
             console.log("tarification ajouté avec succès",res)
           });
    console.log('envoyer2222 : ', this.tarificationForm.value)
  }
  getlistTarifications() {
  
    this.tarificationService.getlistTarifications().subscribe(
      res => {
        this.listTarifications = res.items

        console.log(' test: ', this.listTarifications)
      }
    )
  }
  onchange(event: any) {
    this.pageSize = event.target.value;
    this.tarificationService.getTarificationsPerLimit(this.pageSize).subscribe(
      res => {
        this.listTarifications = res.items
      }
    )

  }
  getTarificationBytype(type: any) {
    this.tarificationService.getTarification(type).subscribe(
      res => {
        if (type == 1) {
          this.tarificationRamassage = res
          console.log("type tarification: ", this.tarificationRamassage)
        }

      }
    )
  }
  next() {
    if (this.page > 0) {
      this.tarificationService.getTarificationsPerPage(this.page + 1, this.pageSize).subscribe(
        res => {
          this.listTarifications = res.items
          if (this.listTarifications == null)
            this.pageSize -= 1

        }
      )
    }

  }
  edit(cbmarq:number){
    this.tarificationService.editTarification(this.newTarifications, cbmarq).subscribe(
      res=>{
        //this.newTarifications=res
        console.log("edit: ",res)
      }
    )
  }
  delete(cbmarq:number){
    this.tarificationService.deleteTarification(cbmarq).subscribe(
      res=>{
    
      }
    )
  }

}
