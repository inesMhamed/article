import { Component, AfterViewInit, ViewChild, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ArticlesService } from 'src/app/shared/articles.service';
import { Articles } from '../../models/articles.model';
import { first } from "rxjs/operators";
import { EditModalComponent } from '../edit-modal/edit-modal.component';
import { MatDialog } from '@angular/material/dialog';




@Component({
  selector: 'app-liste-articles',
  templateUrl: './liste-articles.component.html',
  styleUrls: ['./liste-articles.component.scss'],
  providers: [ArticlesService]

})
export class ListeArticlesComponent implements OnInit {
  newArticles: Articles = new Articles()
  articleRamassage: any[]=[]
  type: number
  page: number
  limit: any
  pageSize: number;
  articleForm: FormGroup;
  submitted = false;
  editForm: FormGroup;
  listArticles: any[] = [];
  cbmarq: any;
  modeEdit :boolean=true
  show: boolean=true;
  constructor(private articleService: ArticlesService, private formBuilder: FormBuilder,private dialog:MatDialog ,) {
    this.pageSize = 10;
    this.page = 1;
    this.type = 1
  }
  get f() {
    return this.articleForm.controls;
  }

  ngOnInit(): void {
   
    this.getlistArticles();
    this.getArticleBytype(this.type)

    this.articleForm = this.formBuilder.group({
      intitule: [''],
      type: [''],
      articleliaison: [''],
    });
    
    this.editForm = this.formBuilder.group({
      intitule: [''],
      type: [''],
      articleliaison: [''],
    });
    
  }
  onSubmitEdit(){

    console.log("form edit :" ,this.editForm.value)
    if (this.editForm.invalid) {
      return;
  }
  this.newArticles.intitule=this.editForm.value.intitule
 /*  this.newArticles.articleliaison= this.editForm.value.articleliaison */
  this.newArticles.type= this.editForm.value.type
  if(!this.editForm.value.articleliaison){
    this.newArticles.articleliaison = null
  }
  else this.newArticles.articleliaison= this.editForm.value.articleliaison
    this.articleService.editArticle(this.newArticles).pipe(first()).subscribe((res) => {
             console.log("article edité avec succès",res)
           });
  }
  onSubmit():void {
    console.log(this.articleForm.value.intitule)
    this.submitted = true;
    if (this.articleForm.invalid) {
      return;
  }
 
  this.newArticles.intitule=this.articleForm.value.intitule
  this.newArticles.articleliaison= this.articleForm.value.articleliaison
  this.newArticles.type= this.articleForm.value.type
  if(!this.articleForm.value.articleliaison){
    this.newArticles.articleliaison = null
  }
    this.articleService.addArticle(this.newArticles).pipe(first()).subscribe((res) => {
             console.log("article ajouté avec succès",res)
           });
  }
  getlistArticles() {
  
    this.articleService.getlistArticles().subscribe(
      res => {
        this.listArticles = res.items
        console.log("page :" ,this.page)
        console.log(' test: ', this.listArticles)
      }
    )
  }
  onchange(event: any) {
    this.pageSize = event.target.value;
    this.articleService.getArticlesPerLimit(this.pageSize).subscribe(
      res => {
        console.log( "on change", res)
        this.listArticles = res.items
      }
    )
  }
  selectChange2(event:any){
    this.type=event.target.value
    if(this.type == 0){
      this.show= false
      this.newArticles.articleliaison= null
      console.log("liaison: ", )
    }
    else{
      this.show= true
      this.newArticles.articleliaison= this.articleForm.value.articleliaison
    }
  }
  selectChange(event:any){
    console.log(event);
    
    console.log("event", (<HTMLInputElement>event.target).value);
    
    this.cbmarq=event.target.value;
    this.articleService.getArticleByCbmarq((<HTMLInputElement>event.target).value).subscribe(
      res => {
        if(this.type==1){
          this.articleRamassage = res

        
        }
        
      })

  }
  getArticleBytype(type: any) {
    this.articleService.getArticlePerType(type).subscribe(
      res => {
        if (type == 1) {
          this.articleRamassage = res
          console.log("type article: ", this.articleRamassage)
        }

      }
    )
  }
  next() {
    if (this.page > 0) {
      this.articleService.getArticlesPerPage(this.page += 1, this.pageSize).subscribe(
        res => {
          this.listArticles = res.items
          console.log("this.page1 :" ,this.page)
          if (this.listArticles == null)
            this.page -= 1
            console.log("this.page :" ,this.page)

        }
      )
    }
  }
  previous() {
    if (this.page > 0) {
      this.articleService.getArticlesPerPage(this.page -= 1, this.pageSize).subscribe(
        res => {
          console.log("precedent :" ,res)
          this.listArticles = res.items
          if (this.listArticles == null)
            this.page += 1

        }
      )
    }
  }
  edit(artic){
  //   this.articleForm.value.intitule
  //   console.log("article",artic);

  //   console.log(this.articleForm.value);
    
  //   if(!artic.articleliaison){
  //   artic.articleliaison = null
    
  //  this.articleService.editArticle(artic).subscribe(
  //   res=>{
  //       this.newArticles=res
  //     console.log("edit: ",res)
  //      }
     
  //  )
  
//  }

  this.dialog.open(EditModalComponent, { data: artic, }).afterClosed().subscribe(() => {
      this. getlistArticles() 
    //console.log('data :',data)
    });
}
  delete(cbmarq:number){
    this.articleService.deleteArticle(cbmarq).subscribe(
      res=>{
    
      }
    )
  }
}
