import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { Articles } from 'src/app/models/articles.model';
import { ArticlesService } from 'src/app/shared/articles.service';
import { first } from "rxjs/operators";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.scss'],
  providers: [ArticlesService]

})
export class EditModalComponent implements OnInit {
  editForm: FormGroup;
  show: boolean=true
  newArticles: Articles = new Articles()
  articleRamassage: any[] = []
  articleliaison = new FormControl('');
  intitule = new FormControl('');
  type = new FormControl('');
  constructor(private articleService: ArticlesService, private formBuilder: FormBuilder, @Inject(MAT_DIALOG_DATA) public defaults: any,
  private dialogRef: MatDialogRef<EditModalComponent>,) { }

  ngOnInit(): void {
    this.getArticleBytype(1)
    console.log(this.defaults);
this.type = this.defaults.type
    this.editForm = this.formBuilder.group({
      cbmarq : ['' || this.defaults.cbmarq],
      intitule: ['' || this.defaults.intitule],
      type: ['' || this.defaults.type],
      articleliaison: [''],
    });
    // if (this.defaults) {
    //   this.editForm = this.formBuilder.group({
    //     intitule: [this.defaults.intitule || ''],
    //     type: [this.type || ''],
    //     articleliaison: [this.articleliaison || ''],
   

    //   })
    // } 

    if (this.defaults) {
      this.editForm = this.formBuilder.group({
        cbmarq: [this.defaults.cbmarq],
       intitule: [this.defaults.intitule || ''],
        type: [this.defaults.type || ''],
       articleliaison: [this.defaults.articleliaison || ''],
        
      })
    } else {
      this.defaults = {} as Articles;
    }
    this.editForm = this.formBuilder.group({
      intitule: [this.defaults.intitule || ''],
      type: [this.defaults.type || ''],
      articleliaison: [this.defaults.articleliaison || ''],
     
    });
   

  }

  onSubmitEdit(){
this.editForm.value.cbmarq = this.defaults.cbmarq
    console.log("form edit :" ,this.editForm.value)
    if (this.editForm.invalid) {
      return;
  }
  this.newArticles.cbmarq = this.defaults.cbmarq
  this.newArticles.intitule=this.editForm.value.intitule
 /*  this.newArticles.articleliaison= this.editForm.value.articleliaison */
  this.newArticles.type= this.editForm.value.type
  if(this.editForm.value.type==1){
    this.editForm.value.articleliaison = null
    this.show=false
  }
  else this.newArticles.articleliaison= this.editForm.value.articleliaison
    this.articleService.editArticle(this.editForm.value).pipe(first()).subscribe((res) => {
             console.log("article edité avec succès",res)   
           });
  }
  getArticleBytype(type: any) {
    this.articleService.getArticlePerType(type).subscribe(
      res => {
        if (type == 1) {
          this.articleRamassage = res
          console.log("type article: ", this.articleRamassage)
        }

      }
    )
  }
}
