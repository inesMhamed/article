import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Articles } from '../models/articles.model';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
/* const optionRequete = {
  headers: new HttpHeaders({ 
    'Access-Control-Allow-Origin':'*',
    'mon-entete-personnalise':'maValeur'
  })
}; */
@Injectable({
  providedIn: 'root'
})
export class ArticlesService {


  constructor(private http: HttpClient) { }


  getlistArticles(): Observable<any> {
    return this.http.get<Articles[]>(`${environment.apiUrl}/articles`)
  }

  getArticleByCbmarq(cbmarq: any) {
    return this.http.get<any[]>(`${environment.apiUrl}/articles/${(cbmarq)}`)
  }
  addArticle(article: Articles): Observable<any> {

    const article2 = '{"intitule": "' + article.intitule + '" ,"type": ' + article.type + ',"articleliaison":' + article.articleliaison + '}'
    console.log("article", article)
    return this.http.post<Articles>(`${environment.apiUrl}/articles`, article2)

  }
  editArticle(article: Articles): Observable<any> {
    console.log(article);
    
    const article2 = '{"intitule": "' + article.intitule + '" ,"type": ' + article.type + ', "articleliaison":' + article.articleliaison + '}'
    console.log("ssssssssss", article.articleliaison)
    console.log(article2);
    
    return this.http.put<any>(`${environment.apiUrl}/articles/${article.cbmarq}`, article2 );
  }

  getArticlesPerPage(page: number, limit: number): Observable<any> {
    return this.http.get<Articles[]>(`${environment.apiUrl}/articles?page=${(page)}&limit=${(limit)}`)
  }
  getArticlesPerLimit(limit: number): Observable<any> {
    return this.http.get<Articles[]>(`${environment.apiUrl}/articles?limit=${(limit)}`)
  }

  getArticlePerType(type: number): Observable<any> {
    return this.http.get<Articles>(`${environment.apiUrl}/articles/type?type=${(type)}`);
  }
  deleteArticle(cbmarq: number): Observable<any> {
    return this.http.delete<Articles>(`${environment.apiUrl}/articles/${(cbmarq)}`);
  }

}
