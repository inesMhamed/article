import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Tarifications } from '../models/tarification.model';

@Injectable({
  providedIn: 'root'
})
export class TarificationsService {

  constructor( private http : HttpClient) { }

  getlistTarifications(): Observable<any> {
    return this.http.get<Tarifications[]>(`${environment.apiUrl}/tarifications`)
  }

  addTarification(tarification : Tarifications): Observable<any> {
      //const headers = new HttpHeaders({'Access-Control-Allow-Origin':'*'});
      //console.log('tarification service:',tarification)
  //let test='{ "intitule": "tarification dimanche", "type": 0,"tarificationliaison": null }'
      return this.http.post<Tarifications>(`${environment.apiUrl}/tarifications1`,tarification)
      
    }
 
  getTarificationsPerPage(page: number, limit: number): Observable<any> {
    return this.http.get<Tarifications[]>(`${environment.apiUrl}/tarifications?page=${(page)}&limit=${(limit)}`)
  }
  getTarificationsPerLimit(limit: number): Observable<any> {
    return this.http.get<Tarifications[]>(`${environment.apiUrl}/tarifications?limit=${(limit)}`)
  }

  getTarification(type: number): Observable<any> {
    return this.http.get<Tarifications>(`${environment.apiUrl}/tarifications/type?type=${(type)}`);
  }
  deleteTarification(cbmarq: number): Observable<any> {
    return this.http.delete<Tarifications>(`${environment.apiUrl}/tarifications/${(cbmarq)}`);
  }
  editTarification(tarification: Tarifications, cbmarq: number): Observable<any> {
    return this.http.put<Tarifications>(`${environment.apiUrl}/tarifications/${(cbmarq)}`,tarification);
  }
}
