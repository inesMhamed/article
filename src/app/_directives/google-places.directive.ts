/// <reference types="@types/googlemaps" />
import { Directive, ElementRef, OnInit, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[google-place]'
})
export class GooglePlacesDirective implements OnInit {
  @Output() placeChange: EventEmitter<any> = new EventEmitter();
  private element: HTMLInputElement;

  constructor(elRef: ElementRef) {
    //elRef will get a reference to the element where
    //the directive is placed
    
    this.element = elRef.nativeElement;
  }

  

  ngOnInit() {
      const autocomplete = new google.maps.places.Autocomplete(this.element);
      //Event listener to monitor place changes in the input
      google.maps.event.addListener(autocomplete, 'place_changed', () => {
      //Emit the new address object for the updated place
      //this.onSelect.emit(this.getFormattedAddress(autocomplete.getPlace()));
   
   const place = autocomplete.getPlace()
   this.placeChange.emit(place)
    });
    autocomplete.setComponentRestrictions({country:'fr'}) 
  }

}
