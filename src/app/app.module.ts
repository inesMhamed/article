import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './components/home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ClientsComponent } from './components/clients/clients.component';
import { ListeArticlesComponent } from './components/liste-articles/liste-articles.component';
import { TarificationComponent } from './components/tarification/tarification.component';
import { HttpClientModule, HTTP_INTERCEPTORS, HttpClient } from "@angular/common/http"; import { ArticlesService } from './shared/articles.service';
import { ZoneComponent } from './components/zone/zone.component';
import { DepotComponent } from './components/depot/depot.component';
import { AgmCoreModule } from '@agm/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JwtInterceptor } from './_helpers/jwt.interceptor';
import { ErrorInterceptor } from './_helpers/error.interceptor';
import { EditModalComponent } from './components/edit-modal/edit-modal.component';

import { MatDialogModule } from '@angular/material/dialog';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    DashboardComponent,
    ClientsComponent,
    ListeArticlesComponent,
    TarificationComponent,
    ZoneComponent,
    DepotComponent,
    EditModalComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    MatDialogModule,

    HttpClientModule,
    AgmCoreModule.forRoot({
      apiKey: "Add google api key",
      libraries: ["places", "geometry"]
    }),
    BrowserAnimationsModule



  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  providers: [HttpClientModule, ArticlesService,
    { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true }
  ],
  bootstrap: [AppComponent],
  entryComponents: [EditModalComponent],
})
export class AppModule { }
