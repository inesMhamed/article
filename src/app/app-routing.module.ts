import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ClientsComponent } from './components/clients/clients.component';
import { DepotComponent } from './components/depot/depot.component';
import {HomeComponent} from './components/home/home.component';
import { ListeArticlesComponent } from './components/liste-articles/liste-articles.component';
import { TarificationComponent } from './components/tarification/tarification.component';
import { ZoneComponent } from './components/zone/zone.component';

const routes: Routes = [
  { path: 'dashboard', component: HomeComponent },
  { path: 'clients', component: ClientsComponent },
  { path: 'articles', component: ListeArticlesComponent },
  { path: 'tarification', component: TarificationComponent },
  { path: 'depot', component: DepotComponent },
  { path: 'zone', component: ZoneComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
